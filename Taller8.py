#Taller8: Dado un conjunto de 25 valores reales diferentes
lista=[3,4,5,7,8,11,40,18,9,17,21,1,33,34,25,12,15,20,29,36,49,43,31,14,27]

#La suma de la lista.
a=0
for _ in lista:
    a=a+_
print("Suma de los números: ", a)

#El promedio de la lista.
b=0
for _ in lista:
    b=b+_
print("Promedio de los números:", b/25)

#La mediana de la lista.
lista.sort()
print(lista)
print("La mediana: ", lista[12])